# Specify a base image as builder
FROM node:alpine as builder

# Specify the working directory
WORKDIR /usr/app

# Install dependencies and ingest node files
COPY ./package.json ./
RUN npm install
COPY ./ ./

# Build up final files

RUN npm run build

# Specify a base image for the final build

FROM nginx
EXPOSE 80

# Ingest build files into the final build

COPY --from=builder /usr/app/build /usr/share/nginx/html